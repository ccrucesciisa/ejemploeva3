/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.ejemploeva3j.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.ejemploeva3j.dao.exceptions.NonexistentEntityException;
import root.ejemploeva3j.dao.exceptions.PreexistingEntityException;
import root.ejemploeva3j.entity.Comunas;

/**
 *
 * @author ccruces
 */
public class ComunasJpaController implements Serializable {

    public ComunasJpaController() {
    
    }
    
    
   EntityManagerFactory emf = Persistence.createEntityManagerFactory("comunas_PU");
    

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Comunas comunas) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(comunas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findComunas(comunas.getNombre()) != null) {
                throw new PreexistingEntityException("Comunas " + comunas + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Comunas comunas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            comunas = em.merge(comunas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = comunas.getNombre();
                if (findComunas(id) == null) {
                    throw new NonexistentEntityException("The comunas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Comunas comunas;
            try {
                comunas = em.getReference(Comunas.class, id);
                comunas.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The comunas with id " + id + " no longer exists.", enfe);
            }
            em.remove(comunas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Comunas> findComunasEntities() {
        return findComunasEntities(true, -1, -1);
    }

    public List<Comunas> findComunasEntities(int maxResults, int firstResult) {
        return findComunasEntities(false, maxResults, firstResult);
    }

    private List<Comunas> findComunasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Comunas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Comunas findComunas(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Comunas.class, id);
        } finally {
            em.close();
        }
    }

    public int getComunasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Comunas> rt = cq.from(Comunas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
