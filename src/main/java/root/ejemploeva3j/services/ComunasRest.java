/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.ejemploeva3j.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.ejemploeva3j.dao.ComunasJpaController;
import root.ejemploeva3j.dao.exceptions.NonexistentEntityException;
import root.ejemploeva3j.entity.Comunas;

/**
 *
 * @author ccruces
 */

@Path("comunas")
public class ComunasRest {


@GET
@Produces(MediaType.APPLICATION_JSON)
public Response listarComunas(){
    
    ComunasJpaController dao=new ComunasJpaController();
    
  List<Comunas> comunas=  dao.findComunasEntities();
  
  return Response.ok(200).entity(comunas).build();
    
    
}

@POST
@Produces(MediaType.APPLICATION_JSON)
public  Response crear(Comunas comuna ){
    
   ComunasJpaController dao=new ComunasJpaController();  
    try {
        dao.create(comuna);
    } catch (Exception ex) {
        Logger.getLogger(ComunasRest.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return Response.ok(200).entity(comuna).build();
    
    
}
@PUT
@Produces(MediaType.APPLICATION_JSON)
public Response actualizar(Comunas comuna){
    
   ComunasJpaController dao=new ComunasJpaController();  
    
    try {
        dao.edit(comuna);
    } catch (Exception ex) {
        Logger.getLogger(ComunasRest.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return Response.ok(200).entity(comuna).build();
    
}

@DELETE
@Path("/{ideliminar}")
@Produces(MediaType.APPLICATION_JSON)
public Response eliminar(@PathParam("ideliminar") String ideliminar){
    
       ComunasJpaController dao=new ComunasJpaController();  
    try {
        dao.destroy(ideliminar);
    } catch (NonexistentEntityException ex) {
        Logger.getLogger(ComunasRest.class.getName()).log(Level.SEVERE, null, ex);
    }
       
       return Response.ok("cliente eliminado").build();
    
    
    }

@GET
@Path("/{idConsultar}")
@Produces(MediaType.APPLICATION_JSON)
public Response consultarPorId(@PathParam("idConsultar") String idConsultar){
    
     ComunasJpaController dao=new ComunasJpaController();  
   Comunas comuna= dao.findComunas(idConsultar);
     
    return Response.ok(200).entity(comuna).build();
    
    
}

    
}
