<%-- 
    Document   : index
    Created on : 10-06-2021, 20:23:16
    Author     : Ripley
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Endpoints Api Comunas</h1>
         <h1>Lista Comunas - GET - https://ejemploeva3.herokuapp.com/api/comunas</h1>
         <h1>Lista Comuna por id - GET - https://ejemploeva3.herokuapp.com/api/comunas/{id}</h1>
         <h1>Crear Comuna - POST - https://ejemploeva3.herokuapp.com/api/comunas</h1>
         <h1>Actualizar Comuna - PUT - https://ejemploeva3.herokuapp.com/api/comunas</h1>
         <h1>Eliminar Comunas - DELETE - https://ejemploeva3.herokuapp.com/api/comunas</h1>
         
    </body>
</html>
